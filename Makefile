INFS = main.c args.c eratosthenes.c common.c
OUTF = primes
WARN = -Wall -Wextra -Wpedantic -Wconversion -Wvla
OPTM = -Ofast -g0
FLGS = -std=c99 -march=native

default:
	$(CC) $(FLGS) $(WARN) $(OPTM) $(INFS) -o $(OUTF)

debug:
	$(CC) $(FLGS) $(WARN) -g3 -pg $(INFS) -o $(OUTF)

clean:
	rm -f ./primes*

