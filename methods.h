#ifndef _h_methods
#define _h_methods

#define _POSIX_C_SOURCE 199309L
#include <time.h>

#include "common.h"

int eratosthenes(_Bool*, unsigned long, struct timespec*);

#endif

