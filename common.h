#ifndef _h_common
#define _h_common

#define _POSIX_C_SOURCE 199309L
#include <time.h>

#define SIZE  ( primes_max * sizeof(_Bool))
#define SIZEP (*primes_max * sizeof(_Bool))

unsigned long isqrt(unsigned long i);
void get_timespec_delta(struct timespec* delta, struct timespec* begin, struct timespec* end);

#endif

