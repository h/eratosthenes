#define _POSIX_C_SOURCE 199309L
#include <time.h>

#include "common.h"

unsigned long isqrt(unsigned long i)
{
	unsigned long l = 0;
	unsigned long r = i + 1;
	while (l != r - 1)
	{
		unsigned long m = (l + r) / 2;
		if (m * m <= i)
			l = m;
		else
			r = m;
	}
	return l;
}

void get_timespec_delta(struct timespec* delta, struct timespec* begin, struct timespec* end)
{
	delta->tv_sec  = end->tv_sec  - begin->tv_sec ;
	delta->tv_nsec = end->tv_nsec - begin->tv_nsec;

	const long NS_PER_S = 1000000000;

	if(delta->tv_sec > 0 && delta->tv_nsec < 0)
	{
		delta->tv_sec  -= 1;
		delta->tv_nsec += NS_PER_S;
	} else if(delta->tv_sec < 0 && delta->tv_nsec > 0)
	{
		delta->tv_sec  += 1;
		delta->tv_nsec -= NS_PER_S;
	}
}

