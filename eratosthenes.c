#define _POSIX_C_SOURCE 199309L
#include <time.h>

#include "common.h"
#include "methods.h"

int eratosthenes(_Bool* primes, unsigned long primes_max, struct timespec* begin)
{
	int ret = clock_gettime(CLOCK_MONOTONIC_RAW, begin);

	for(unsigned long i = 2; i <= isqrt(primes_max); ++i)
	{
		if(primes[i])
		{
			for(unsigned long c = 2, j = 0; j < primes_max; j = (i * c), ++c)
			{
				if(primes[j])
					primes[j] = 0;
			}
		}
	}

	return ret;
}

